package com.itjuana.library.rest;

import com.itjuana.library.application.Library;
import com.itjuana.library.domain.author.Author;
import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.author.FirstName;
import com.itjuana.library.domain.author.LastName;
import com.itjuana.library.domain.book.*;
import com.itjuana.library.infrastructure.BookRepositoryMemory;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.yaml.snakeyaml.util.UriEncoder;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@MicronautTest
public class BookControllerTest {

    // Defining a Mocked version of the BookRepository
    @MockBean(BookRepositoryMemory.class)
    BookRepository bookRepository() {
        return mock(BookRepository.class);
    }

    @Inject
    BookRepository bookRepository;

    @Inject
    Library library;

    @Inject
    @Client("/")
    RxHttpClient client;

    @Test
    void testFindByISBNReturnBookTitle() {
        // Given
        String isbn = "abcd";
        when(bookRepository.findByISBN(new BookISBN(isbn)))
                .thenReturn(
                        new Book(
                                new BookId(1),
                                new BookTitle("Awesome Title"),
                                new BookISBN(isbn),
                                new Author(
                                        new AuthorId(1),
                                        new FirstName("Foo"),
                                        new LastName("Bar")
                                )
                        )
                );
        // When
        String response = client.toBlocking().retrieve(HttpRequest.GET("/book/findByISBN/" + isbn), String.class);
        // Then
        Assertions.assertEquals(response, "Awesome Title by Foo Bar");
    }

    @Test
    void testFindByTitleReturnBookTitleAndAuthorNamePerLine() {
        // Given
        String title = "How to Cook Everything";
        when(bookRepository.findByTitle(new BookTitle(title)))
                .thenReturn(
                    Arrays.asList(
                            new Book(
                                    new BookId(1),
                                    new BookTitle(title),
                                    new BookISBN("1234"),
                                    new Author(
                                            new AuthorId(1),
                                            new FirstName("Foo"),
                                            new LastName("Bar")
                                    )
                            ),
                            new Book(
                                    new BookId(2),
                                    new BookTitle(title),
                                    new BookISBN("5678"),
                                    new Author(
                                            new AuthorId(2),
                                            new FirstName("Another"),
                                            new LastName("Guy")
                                    )
                            )
                    )
                );
        // When
        String response = client.toBlocking().retrieve(HttpRequest.GET("/book/findByTitle/" + UriEncoder.encode(title)), String.class);
        // Then
        StringBuilder expectedResponse = new StringBuilder();
        expectedResponse.append(title + " by Foo Bar\n");
        expectedResponse.append(title + " by Another Guy\n");
        Assertions.assertEquals(response, expectedResponse.toString());
    }

    @Test
    void testFindByAuthorReturnBookTitleAndAuthorNamePerLine() {
        // Given
        long authorId = 100;
        when(bookRepository.findByAuthor(new AuthorId(authorId)))
                .thenReturn(
                        Arrays.asList(
                                new Book(
                                        new BookId(200),
                                        new BookTitle("The Universe in a Nutshell"),
                                        new BookISBN("9780"),
                                        new Author(
                                                new AuthorId(authorId),
                                                new FirstName("Stephen"),
                                                new LastName("Hawking")
                                        )
                                ),
                                new Book(
                                        new BookId(300),
                                        new BookTitle("Black Holes and Baby Universes"),
                                        new BookISBN("0553"),
                                        new Author(
                                                new AuthorId(authorId),
                                                new FirstName("Stephen"),
                                                new LastName("Hawking")
                                        )
                                )
                        )
                );
        // When
        String response = client.toBlocking().retrieve(HttpRequest.GET("/book/findByAuthor/" + authorId), String.class);
        // Then
        StringBuilder expectedResponse = new StringBuilder();
        expectedResponse.append("The Universe in a Nutshell by Stephen Hawking\n");
        expectedResponse.append("Black Holes and Baby Universes by Stephen Hawking\n");
        Assertions.assertEquals(response, expectedResponse.toString());
    }
}
