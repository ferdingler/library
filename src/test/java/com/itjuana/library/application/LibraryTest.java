package com.itjuana.library.application;


import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.book.BookISBN;
import com.itjuana.library.domain.book.BookRepository;
import com.itjuana.library.domain.book.BookTitle;
import com.itjuana.library.infrastructure.BookRepositoryMemory;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@MicronautTest
public class LibraryTest {

    // Defining a Mocked version of the BookRepository
    @MockBean(BookRepositoryMemory.class)
    BookRepository bookRepository() {
        return mock(BookRepository.class);
    }

    @Inject
    BookRepository bookRepository;

    @Inject
    Library library;

    @Test
    void testVerifyFindByTitleHasBeenCalled() {
        BookTitle bookTitle = new BookTitle("Foo");
        library.findByTitle(bookTitle);
        verify(bookRepository).findByTitle(bookTitle);
    }

    @Test
    void testVerifyFindByAuthorHasBeenCalled() {
        AuthorId authorId = new AuthorId(123);
        library.findByAuthor(authorId);
        verify(bookRepository).findByAuthor(authorId);
    }

    @Test
    void testVerifyFindByISBNHasBeenCalled() {
        BookISBN bookISBN = new BookISBN("abcd");
        library.findByISBN(bookISBN);
        verify(bookRepository).findByISBN(bookISBN);
    }
}
