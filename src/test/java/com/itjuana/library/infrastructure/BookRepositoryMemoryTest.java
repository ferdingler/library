package com.itjuana.library.infrastructure;

import com.itjuana.library.domain.author.Author;
import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.book.Book;
import com.itjuana.library.domain.book.BookISBN;
import com.itjuana.library.domain.book.BookRepository;
import com.itjuana.library.domain.book.BookTitle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class BookRepositoryMemoryTest {

    @Test
    void testFindBookByISBNBookExists() {
        // Given
        BookRepositoryMemory repo = new BookRepositoryMemory();
        // When
        Book book = repo.findByISBN(new BookISBN("9780"));
        // Then
        Assertions.assertNotNull(book);
        Assertions.assertEquals(book.getBookTitle().getValue(), "Domain-Driven Design: Tackling Complexity in the Heart of Software");
    }

    @Test
    void testFindBookByTitle() {
        // Given
        BookRepository repo = new BookRepositoryMemory();
        // When
        List<Book> books = repo.findByTitle(new BookTitle("Life After Life: A Novel"));
        // Then
        Assertions.assertEquals(books.size(), 2);
    }

    @Test
    void testFindBookByAuthor() {
        // Given
        BookRepository repo = new BookRepositoryMemory();
        // When
        List<Book> books = repo.findByAuthor(new AuthorId(1));
        // Then
        Assertions.assertEquals(books.size(), 2);
    }

}
