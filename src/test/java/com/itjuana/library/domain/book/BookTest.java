package com.itjuana.library.domain.book;

import com.itjuana.library.domain.author.Author;
import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.author.FirstName;
import com.itjuana.library.domain.author.LastName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookTest {

    @Test
    void testSettersAndGetters() {
        // Given
        BookId id = new BookId(123);
        BookTitle title = new BookTitle("The great title");
        BookISBN isbn = new BookISBN("abcd");
        Author author = new Author(new AuthorId(123), new FirstName("Eric"), new LastName("Evans"));
        Book book = new Book(id, title, isbn, author);

        // Then
        Assertions.assertEquals(book.getBookId().getValue(), 123);
        Assertions.assertEquals(book.getBookTitle().getValue(), "The great title");
        Assertions.assertEquals(book.getBookISBN().getValue(), "abcd");
        Assertions.assertEquals(book.getAuthor(), author);
    }

    @Test
    void testBookWithSameValueShouldBeEquals() {
        // Given
        BookId id = new BookId(123);
        BookTitle title = new BookTitle("The great title");
        BookISBN isbn = new BookISBN("abcd");
        Author author = new Author(new AuthorId(123), new FirstName("Eric"), new LastName("Evans"));
        Book book1 = new Book(id, title, isbn, author);
        Book book2 = new Book(id, title, isbn, author);

        // Then
        Assertions.assertEquals(book1, book2);
    }

}
