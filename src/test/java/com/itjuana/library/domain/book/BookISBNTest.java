package com.itjuana.library.domain.book;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookISBNTest {

    @Test
    void testISBNShouldNotBeNull() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookISBN isbn = new BookISBN(null);
        });
        Assertions.assertEquals(e.getMessage(), "ISBN should not be null");
    }

    @Test
    void testISBNShouldHave4Characters() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookISBN isbn = new BookISBN("123");
        });
        Assertions.assertEquals(e.getMessage(), "ISBN should have 4 characters");
    }
}
