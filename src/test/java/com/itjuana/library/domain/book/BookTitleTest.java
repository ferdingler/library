package com.itjuana.library.domain.book;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookTitleTest {

    @Test
    void testTitleShouldNotBeNull() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookTitle title = new BookTitle(null);
        });
        Assertions.assertEquals(e.getMessage(), "Title should not be null");
    }

    @Test
    void testTitleShouldNotBeEmpty() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookTitle title = new BookTitle("      ");
        });
        Assertions.assertEquals(e.getMessage(), "Title should not be empty");
    }

    @Test
    void testDifferentObjectsWithSameTitleShouldBeEqual() {
        // Given
        BookTitle title1 = new BookTitle("Domain-Driven Design: Tackling Complexity in the Heart of Software");
        BookTitle title2 = new BookTitle("Domain-Driven Design: Tackling Complexity in the Heart of Software");
        // Then
        Assertions.assertEquals(title1, title2);
    }

    @Test
    void testToStringShouldReturnTitle() {
        BookTitle title = new BookTitle("Domain-Driven Design: Tackling Complexity in the Heart of Software");
        Assertions.assertEquals(title.toString(), "Domain-Driven Design: Tackling Complexity in the Heart of Software");
    }
}
