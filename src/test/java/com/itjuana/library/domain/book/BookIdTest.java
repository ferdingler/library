package com.itjuana.library.domain.book;

import com.itjuana.library.domain.author.AuthorId;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BookIdTest {

    @Test
    void testIdShouldBeGreaterThanZero() {
        long randomValue = new RandomDataGenerator().nextLong(1, 100);
        BookId bookId = new BookId(randomValue);
        Assertions.assertEquals(bookId.getValue(), randomValue);
    }

    @Test
    void testIdShouldRaiseExceptionWhenValueIsLessThanZero() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookId bookId = new BookId(-23);
        });
        Assertions.assertEquals(e.getMessage(), "Value can't be lower than zero");
    }

    @Test
    void testIdShouldRaiseExceptionWhenValueIsZero() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            BookId bookId = new BookId(0);
        });
        Assertions.assertEquals(e.getMessage(), "Value can't be zero");
    }

    @Test
    void testIdWithSameValueShouldBeEquals() {
        long randomValue = new RandomDataGenerator().nextLong(1, 100);
        Assertions.assertEquals(
                new BookId(randomValue), new BookId(randomValue)
        );
    }

    @Test
    void testIdToStringReturnValue() {
        BookId bookId = new BookId(1988);
        Assertions.assertEquals(bookId.toString(), "1988");
    }

}
