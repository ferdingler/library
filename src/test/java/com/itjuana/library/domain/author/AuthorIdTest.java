package com.itjuana.library.domain.author;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AuthorIdTest {

    @Test
    void testIdShouldBeGreaterThanZero() {
        long randomValue = new RandomDataGenerator().nextLong(1, 100);
        AuthorId authorId = new AuthorId(randomValue);
        Assertions.assertEquals(authorId.getValue(), randomValue);
    }

    @Test
    void testIdShouldRaiseExceptionWhenValueIsLessThanZero() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            AuthorId authorId = new AuthorId(-23);
        });
        Assertions.assertEquals(e.getMessage(), "Value can't be lower than zero");
    }

    @Test
    void testIdShouldRaiseExceptionWhenValueIsZero() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            AuthorId authorId = new AuthorId(0);
        });
        Assertions.assertEquals(e.getMessage(), "Value can't be zero");
    }

    @Test
    void testIdWithSameValueShouldBeEquals() {
        long randomValue = new RandomDataGenerator().nextLong(1, 100);
        Assertions.assertEquals(
                new AuthorId(randomValue), new AuthorId(randomValue)
        );
    }

    @Test
    void testIdToStringReturnValue() {
        AuthorId authorId = new AuthorId(1988);
        Assertions.assertEquals(authorId.toString(), "1988");
    }

}
