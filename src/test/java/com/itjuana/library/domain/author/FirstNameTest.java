package com.itjuana.library.domain.author;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FirstNameTest {

    @Test
    void testValueShouldNotBeNull() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            FirstName firstName = new FirstName(null);
        });
        Assertions.assertEquals(e.getMessage(), "Value should not be null");
    }

    @Test
    void testValueShouldNotBeEmpty() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            FirstName firstName = new FirstName("");
        });
        Assertions.assertEquals(e.getMessage(), "Value should not be empty");
    }

    @Test
    void testValueSpacesShouldBeRemovedAtTheBeginning() {
        FirstName firstName = new FirstName("   Eric");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueSpacesShouldBeRemovedAtTheEnd() {
        FirstName firstName = new FirstName("Eric     ");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueSpacesShouldBeRemovedInBothSides() {
        FirstName firstName = new FirstName("          Eric     ");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueShouldBeTitleCasedAllLowercase() {
        FirstName firstName = new FirstName("eric");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueShouldBeTitleCasedAllUppercase() {
        FirstName firstName = new FirstName("ERIC");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueShouldBeTitleCasedAllUppercaseWithSpaces() {
        FirstName firstName = new FirstName("  eRIc  ");
        Assertions.assertEquals(firstName.getValue(), "Eric");
    }

    @Test
    void testValueToStringReturnValue() {
        FirstName firstName = new FirstName("Eric");
        Assertions.assertEquals(firstName.toString(), "Eric");
    }

//    // BUG: See note in the LastName.java implementation
//    @Test
//    void testValueShouldNotAcceptStringsWithSpacesOnly() {
//        LastName lastName = new LastName("    ");
//    }

}
