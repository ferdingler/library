package com.itjuana.library.domain.author;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LastNameTest {

    @Test
    void testValueShouldNotBeNull() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
           LastName lastName = new LastName(null);
        });
        Assertions.assertEquals(e.getMessage(), "Value should not be null");
    }

    @Test
    void testValueShouldNotBeEmpty() {
        RuntimeException e = Assertions.assertThrows(RuntimeException.class, () -> {
            LastName lastName = new LastName("");
        });
        Assertions.assertEquals(e.getMessage(), "Value should not be empty");
    }

    @Test
    void testValueSpacesShouldBeRemovedAtTheBeginning() {
        LastName lastName = new LastName("   Evans");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueSpacesShouldBeRemovedAtTheEnd() {
        LastName lastName = new LastName("Evans     ");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueSpacesShouldBeRemovedInBothSides() {
        LastName lastName = new LastName("          Evans     ");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueShouldBeTitleCasedAllLowercase() {
        LastName lastName = new LastName("evans");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueShouldBeTitleCasedAllUppercase() {
        LastName lastName = new LastName("EVANS");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueShouldBeTitleCasedAllUppercaseWithSpaces() {
        LastName lastName = new LastName("  eVANs  ");
        Assertions.assertEquals(lastName.getValue(), "Evans");
    }

    @Test
    void testValueToStringReturnValue() {
        LastName lastName = new LastName("Evans");
        Assertions.assertEquals(lastName.toString(), "Evans");
    }

//    // BUG: See note in the LastName.java implementation
//    @Test
//    void testValueShouldNotAcceptStringsWithSpacesOnly() {
//        LastName lastName = new LastName("    ");
//    }

}
