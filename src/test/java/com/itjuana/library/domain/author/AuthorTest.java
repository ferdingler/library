package com.itjuana.library.domain.author;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AuthorTest {

    @Test
    void testCheckSettersAndGetters() {
        // Given
        AuthorId authorId = new AuthorId(1287);
        FirstName firstName = new FirstName("Eric");
        LastName lastName = new LastName("Evans");
        Author author = new Author(authorId, firstName, lastName);
        // Then
        Assertions.assertEquals(author.getAuthorId().getValue(), 1287);
        Assertions.assertEquals(author.getFirstName().getValue(), "Eric");
        Assertions.assertEquals(author.getLastName().getValue(), "Evans");
    }

    @Test
    void testAuthorWithSameValuesShouldBeTheSame() {
        // Given
        AuthorId authorId1 = new AuthorId(1287);
        FirstName firstName1 = new FirstName("Eric");
        LastName lastName1 = new LastName("Evans");
        Author author1 = new Author(authorId1, firstName1, lastName1);

        AuthorId authorId2 = new AuthorId(1287);
        FirstName firstName2 = new FirstName("Eric");
        LastName lastName2 = new LastName("Evans");
        Author author2 = new Author(authorId2, firstName2, lastName2);

        // Then
        Assertions.assertEquals(author1, author2);
    }

    @Test
    void testAuthorWithDifferentIdShouldNotBeTheSame() {
        // Given
        AuthorId authorId1 = new AuthorId(1287);
        FirstName firstName1 = new FirstName("Eric");
        LastName lastName1 = new LastName("Evans");
        Author author1 = new Author(authorId1, firstName1, lastName1);

        AuthorId authorId2 = new AuthorId(9999); // Different AuthorId
        FirstName firstName2 = new FirstName("Eric");
        LastName lastName2 = new LastName("Evans");
        Author author2 = new Author(authorId2, firstName2, lastName2);

        // Then
        Assertions.assertNotEquals(author1, author2);
    }

    @Test
    void testAuthorWithDifferentFirstNameShouldNotBeTheSame() {
        // Given
        AuthorId authorId1 = new AuthorId(1287);
        FirstName firstName1 = new FirstName("Eric");
        LastName lastName1 = new LastName("Evans");
        Author author1 = new Author(authorId1, firstName1, lastName1);

        AuthorId authorId2 = new AuthorId(1287);
        FirstName firstName2 = new FirstName("Another"); // Different FirstName
        LastName lastName2 = new LastName("Evans");
        Author author2 = new Author(authorId2, firstName2, lastName2);

        // Then
        Assertions.assertNotEquals(author1, author2);
    }

    @Test
    void testAuthorWithDifferentLastNameShouldNotBeTheSame() {
        // Given
        AuthorId authorId1 = new AuthorId(1287);
        FirstName firstName1 = new FirstName("Eric");
        LastName lastName1 = new LastName("Evans");
        Author author1 = new Author(authorId1, firstName1, lastName1);

        AuthorId authorId2 = new AuthorId(1287);
        FirstName firstName2 = new FirstName("Eric");
        LastName lastName2 = new LastName("Another"); // Different LastName
        Author author2 = new Author(authorId2, firstName2, lastName2);

        // Then
        Assertions.assertNotEquals(author1, author2);
    }
}
