package com.itjuana.library.infrastructure;

import com.itjuana.library.domain.author.Author;
import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.author.FirstName;
import com.itjuana.library.domain.author.LastName;
import com.itjuana.library.domain.book.*;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Singleton
public class BookRepositoryMemory implements BookRepository {

    List<Book> books;

    // This is a dumb and inefficient example
    public BookRepositoryMemory() {
        this.books = Arrays.asList(
                new Book(
                        new BookId(1),
                        new BookTitle("Domain-Driven Design: Tackling Complexity in the Heart of Software"),
                        new BookISBN("9780"),
                        new Author(
                                new AuthorId(1),
                                new FirstName("Eric"),
                                new LastName("Evans")
                        )
                ),
                new Book(
                        new BookId(2),
                        new BookTitle("Clean Architecture: A Craftsman's Guide to Software Structure and Design"),
                        new BookISBN("0134"),
                        new Author(
                                new AuthorId(2),
                                new FirstName("Robert"),
                                new LastName("Martin")
                        )
                ),
                new Book(
                        new BookId(3),
                        new BookTitle("The DevOps Handbook"),
                        new BookISBN("1942"),
                        new Author(
                                new AuthorId(3),
                                new FirstName("Gene"),
                                new LastName("Kim")
                        )
                ),
                new Book(
                        new BookId(4),
                        new BookTitle("Domain-Driven Design Reference: Definitions and Pattern Summaries"),
                        new BookISBN("1457"),
                        new Author(
                                new AuthorId(1),
                                new FirstName("Eric"),
                                new LastName("Evans")
                        )
                ),
                new Book(
                        new BookId(5),
                        new BookTitle("Life After Life: A Novel"),
                        new BookISBN("0316"),
                        new Author(
                                new AuthorId(4),
                                new FirstName("Jill"),
                                new LastName("McCorkle")
                        )
                ),
                new Book(
                        new BookId(6),
                        new BookTitle("Life After Life: A Novel"),
                        new BookISBN("9781"),
                        new Author(
                                new AuthorId(5),
                                new FirstName("Kate"),
                                new LastName("Atkinson")
                        )
                )
        );
    }

    @Override
    public List<Book> findByTitle(BookTitle bookTitle) {
        List<Book> books = new ArrayList<Book>();
        // Can you optimize this with the Stream API?
        for(int i=0; i<this.books.size(); i++) {
            if(this.books.get(i).getBookTitle().equals(bookTitle))
                books.add(this.books.get(i));
        }
        return books;
    }

    @Override
    public List<Book> findByAuthor(AuthorId authorId) {
        List<Book> books = new ArrayList<Book>();
        // Can you optimize this with the Stream API?
        for(int i=0; i<this.books.size(); i++) {
            if(this.books.get(i).getAuthor().getAuthorId().equals(authorId))
                books.add(this.books.get(i));
        }
        return books;
    }

    @Override
    public Book findByISBN(BookISBN bookISBN) {
        // Can you optimize this with the Stream API?
        for(int i=0; i<this.books.size(); i++) {
            if(this.books.get(i).getBookISBN().equals(bookISBN))
                return this.books.get(i);
        }
        return null;
    }
}
