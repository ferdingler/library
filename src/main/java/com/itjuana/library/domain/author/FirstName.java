package com.itjuana.library.domain.author;

import java.util.Objects;

// Bonus:
// Known bugs on this class: Someone reported if you initialize the class with two or more empty spaces, it will crash.
// java.lang.StringIndexOutOfBoundsException: String index out of range: 1
// Can you fix it?

public class FirstName {

    private String value;

    public FirstName(String value) {
        // If you want to go further, you can even create custom exceptions related to the domain
        if (value == null) throw new RuntimeException("Value should not be null");
        else if(value.equals("")) throw new RuntimeException("Value should not be empty");
        this.value = value.trim();
        this.value = this.value.substring(0, 1).toUpperCase() + this.value.substring(1).toLowerCase();
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        FirstName firstName = (FirstName) o;
        return this.value.equals(firstName.getValue());
    }

}
