package com.itjuana.library.domain.author;

import java.util.Objects;

public class Author {

    private AuthorId authorId;
    private FirstName firstName;
    private LastName lastName;

    public Author(AuthorId authorId, FirstName firstName, LastName lastName) {
        this.authorId = authorId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public AuthorId getAuthorId() {
        return this.authorId;
    }

    public FirstName getFirstName() {
        return this.firstName;
    }

    public LastName getLastName() {
        return this.lastName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.authorId, this.firstName, this.lastName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return this.authorId.equals(author.getAuthorId()) &&
                this.firstName.equals(author.getFirstName()) &&
                this.lastName.equals(author.getLastName());
    }

}
