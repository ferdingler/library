package com.itjuana.library.domain.author;

import java.util.Objects;

public class AuthorId {

    private long value;

    public AuthorId(long value) {
        // This code can be refactored with <= 0, Can you try?
        // Also, if you want to go further, you can even create custom exceptions related to the domain
        if (value < 0) throw new RuntimeException("Value can't be lower than zero");
        else if (value == 0) throw new RuntimeException("Value can't be zero");
        this.value = value;
    }

    public long getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return String.format("%d", this.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        AuthorId authorId = (AuthorId) o;
        return this.value == authorId.getValue();
    }

}
