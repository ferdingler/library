package com.itjuana.library.domain.book;

import java.util.Objects;

public class BookISBN {

    private String value;

    public BookISBN(String value) {
        if(value == null) throw new RuntimeException("ISBN should not be null");
        this.value = value.trim();
        if(this.value.length() != 4) throw new RuntimeException("ISBN should have 4 characters");
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        BookISBN bookTitle = (BookISBN) o;
        return this.value.equals(bookTitle.getValue());
    }
}
