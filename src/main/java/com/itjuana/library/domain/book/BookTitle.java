package com.itjuana.library.domain.book;

import java.util.Objects;

public class BookTitle {

    private String value;

    public BookTitle(String value) {
        if(value == null) throw new RuntimeException("Title should not be null");
        this.value = value.trim();
        if(this.value.equals("")) throw new RuntimeException("Title should not be empty");
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        BookTitle bookTitle = (BookTitle) o;
        return this.value.equals(bookTitle.getValue());
    }
}
