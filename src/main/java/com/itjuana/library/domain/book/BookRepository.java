package com.itjuana.library.domain.book;

import com.itjuana.library.domain.author.AuthorId;

import java.util.List;

public interface BookRepository {

    List<Book> findByTitle(BookTitle bookTitle);
    List<Book> findByAuthor(AuthorId authorId);
    Book findByISBN(BookISBN bookISBN);

}
