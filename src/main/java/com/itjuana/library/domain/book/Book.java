package com.itjuana.library.domain.book;

import com.itjuana.library.domain.author.Author;

import java.util.Objects;

public class Book {

    private BookId bookId;
    private BookTitle bookTitle;
    private BookISBN bookISBN;
    private Author author;

    public Book(BookId bookId, BookTitle bookTitle, BookISBN bookISBN, Author author) {
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.bookISBN = bookISBN;
        this.author = author;
    }

    public BookId getBookId() { return this.bookId; }
    public BookTitle getBookTitle() { return this.bookTitle; }
    public BookISBN getBookISBN() { return this.bookISBN; }
    public Author getAuthor() { return this.author; }

    @Override
    public int hashCode() {
        return Objects.hash(this.bookId, this.bookTitle, this.bookISBN, this.author);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return this.bookId.equals(book.getBookId()) &&
                this.bookTitle.equals(book.getBookTitle()) &&
                this.bookISBN.equals(book.getBookISBN()) &&
                this.author.equals(book.getAuthor());
    }
}
