package com.itjuana.library.rest;

import com.itjuana.library.application.Library;
import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.book.Book;
import com.itjuana.library.domain.book.BookISBN;
import com.itjuana.library.domain.book.BookTitle;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

import javax.inject.Inject;
import java.util.List;

@Controller("/book")
public class BookController {

    private Library library;

    @Inject
    public BookController(Library library) {
        this.library = library;
    }

    @Get(uri = "/findByISBN/{isbn}", processes = MediaType.TEXT_PLAIN)
    String findByISBN(String isbn) {
        Book book = this.library.findByISBN(new BookISBN(isbn));
        return book.getBookTitle().getValue() + " by " + book.getAuthor().getFirstName() + " " + book.getAuthor().getLastName();
    }

    @Get(uri = "/findByTitle/{title}", processes = MediaType.TEXT_PLAIN)
    String findByTitle(String title) {
        List<Book> books = this.library.findByTitle(new BookTitle(title));
        StringBuilder response = new StringBuilder();
        for(int i=0; i<books.size(); i++) {
            Book book = books.get(i);
            response.append(book.getBookTitle().getValue() + " by " + book.getAuthor().getFirstName() + " " + book.getAuthor().getLastName() + "\n");
        }
        return response.toString();
    }

    @Get(uri = "/findByAuthor/{authorId}", processes = MediaType.TEXT_PLAIN)
    String findByAuthor(long authorId) {
        List<Book> books = this.library.findByAuthor(new AuthorId(authorId));
        StringBuilder response = new StringBuilder();
        for(int i=0; i<books.size(); i++) {
            Book book = books.get(i);
            response.append(book.getBookTitle().getValue() + " by " + book.getAuthor().getFirstName() + " " + book.getAuthor().getLastName() + "\n");
        }
        return response.toString();
    }

}
