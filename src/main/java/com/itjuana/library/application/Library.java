package com.itjuana.library.application;

import com.itjuana.library.domain.author.AuthorId;
import com.itjuana.library.domain.book.Book;
import com.itjuana.library.domain.book.BookISBN;
import com.itjuana.library.domain.book.BookRepository;
import com.itjuana.library.domain.book.BookTitle;

import javax.inject.Inject;
import java.util.List;

public class Library {

    private BookRepository bookRepository;

    @Inject
    public Library(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> findByTitle(BookTitle bookTitle) {
        return bookRepository.findByTitle(bookTitle);
    }

    public List<Book> findByAuthor(AuthorId authorId) {
        return bookRepository.findByAuthor(authorId);
    }

    public Book findByISBN(BookISBN bookISBN) {
        return bookRepository.findByISBN(bookISBN);
    }
}
